package ru.t1.sarychevv.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.sarychevv.tm.api.endpoint.*;
import ru.t1.sarychevv.tm.api.repository.ICommandRepository;
import ru.t1.sarychevv.tm.api.service.*;
import ru.t1.sarychevv.tm.command.AbstractCommand;
import ru.t1.sarychevv.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.sarychevv.tm.exception.system.CommandNotSupportedException;
import ru.t1.sarychevv.tm.repository.CommandRepository;
import ru.t1.sarychevv.tm.service.CommandService;
import ru.t1.sarychevv.tm.service.LoggerService;
import ru.t1.sarychevv.tm.service.PropertyService;
import ru.t1.sarychevv.tm.service.TokenService;
import ru.t1.sarychevv.tm.util.SystemUtil;
import ru.t1.sarychevv.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.sarychevv.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    @Getter
    private final ITokenService tokenService = new TokenService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    @Getter
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    @Getter
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    @Getter
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance();

    @NotNull
    @Getter
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    @Getter
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    public void start(@Nullable String[] args) {
        processArguments(args);

        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @Nullable final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void prepareStartup() {
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initFileScanner();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
    }

    private void prepareShutdown() {
        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
        fileScanner.close();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

}
