package ru.t1.sarychevv.tm.repository.dto;

import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.api.service.dto.IProjectDTOService;
import ru.t1.sarychevv.tm.api.service.dto.ITaskDTOService;
import ru.t1.sarychevv.tm.api.service.dto.IUserDTOService;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.marker.DBCategory;
import ru.t1.sarychevv.tm.service.ConnectionService;
import ru.t1.sarychevv.tm.service.PropertyService;
import ru.t1.sarychevv.tm.service.dto.ProjectDTOService;
import ru.t1.sarychevv.tm.service.dto.TaskDTOService;
import ru.t1.sarychevv.tm.service.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Category(DBCategory.class)
public class SessionDTORepositoryTest extends TestCase {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";

    @NotNull
    private static ISessionDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDTORepository(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @Before
    public void setUp() throws Exception {
        @NotNull final UserDTO user = new UserDTO();
        user.setPassword("user_password");
        user.setLogin("user_login");
        user.setRole(Role.USUAL);
        user.setEmail("user_email");
        user.setFirstName("user_first_name");
        user.setLastName("user_last_name");
        user.setMiddleName("user_middle_name");
        userService.add(user);
        userId = user.getId();

        @NotNull final UserDTO admin = new UserDTO();
        admin.setPassword("admin_password");
        admin.setLogin("admin_login");
        admin.setRole(Role.ADMIN);
        admin.setEmail("admin_email");
        admin.setFirstName("admin_first_name");
        admin.setLastName("admin_last_name");
        admin.setMiddleName("admin_middle_name");
        userService.add(admin);
        adminId = admin.getId();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDTORepository repository = getRepository(entityManager);
            @NotNull final SessionDTO session_first = new SessionDTO();
            session_first.setName("first_test_project_name");
            session_first.setDescription("first_test_project_description");
            @NotNull final SessionDTO session_second = new SessionDTO();
            session_second.setName("first_test_project_name");
            session_second.setDescription("first_test_project_description");
            entityManager.getTransaction().begin();
            repository.add(userId, session_first);
            repository.add(userId, session_second);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @After
    public void tearDown() throws Exception {
        @Nullable final UserDTO user = userService.findOneById(userId);
        if (user != null) userService.removeOne(user);

        @Nullable final UserDTO admin = userService.findOneById(adminId);
        if (admin != null) userService.removeOne(user);

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            repository.removeAll(adminId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @Test
    public void add() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDTORepository repository = getRepository(entityManager);
            @NotNull final SessionDTO session_third = new SessionDTO();
            session_third.setName("first_test_project_name");
            session_third.setDescription("first_test_project_description");
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(userId, session_third));
            entityManager.getTransaction().commit();
            @Nullable final SessionDTO session = repository.findOneById(userId, session_third.getId());
            Assert.assertNotNull(session);
            Assert.assertEquals(session_third.getId(), session.getId());
            Assert.assertEquals(userId, session.getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<SessionDTO> sessions = repository.findAll(userId);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(userId, session.getUserId()));
        entityManager.close();
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(userId, UUID.randomUUID().toString()));
        entityManager.close();
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<SessionDTO> sessions = repository.findAll(userId);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(userId, session.getUserId()));
        entityManager.close();
    }

    @Test
    public void getSize() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ISessionDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(2, repository.getSize(userId));
        entityManager.close();
    }


    @Test
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
            Assert.assertEquals(0, repository.getSize(userId));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
}