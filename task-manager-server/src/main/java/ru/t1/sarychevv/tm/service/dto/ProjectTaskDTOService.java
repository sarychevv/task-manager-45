package ru.t1.sarychevv.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.service.dto.IProjectDTOService;
import ru.t1.sarychevv.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.sarychevv.tm.api.service.dto.ITaskDTOService;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.ProjectIdEmptyException;
import ru.t1.sarychevv.tm.exception.field.TaskIdEmptyException;
import ru.t1.sarychevv.tm.exception.field.UserIdEmptyException;

import java.util.List;

public final class ProjectTaskDTOService implements IProjectTaskDTOService {

    @NotNull
    private final IProjectDTOService projectService;

    @NotNull
    private final ITaskDTOService taskService;

    public ProjectTaskDTOService(@NotNull final IProjectDTOService projectService,
                                 @NotNull final ITaskDTOService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @NotNull
    @Override
    public TaskDTO bindTaskToProject(@Nullable final String userId,
                                     @Nullable final String projectId,
                                     @Nullable final String taskId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskService.updateProjectIdById(userId, taskId, projectId);
        return task;
    }

    @Override
    public void removeProjectById(@Nullable final String userId,
                                  @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks != null) {
            tasks.forEach(m -> {
                try {
                    taskService.removeOneById(userId, m.getId());
                } catch (Exception e) {
                }
            });
        }
        projectService.removeOneById(userId, projectId);
    }

    @NotNull
    @Override
    public TaskDTO unbindTaskFromProject(@Nullable final String userId,
                                         @Nullable final String projectId,
                                         @Nullable final String taskId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskService.updateProjectIdById(userId, taskId, null);
        return task;
    }

}

