package ru.t1.sarychevv.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.endpoint.*;
import ru.t1.sarychevv.tm.api.service.*;
import ru.t1.sarychevv.tm.api.service.dto.*;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.endpoint.*;
import ru.t1.sarychevv.tm.service.AuthService;
import ru.t1.sarychevv.tm.service.ConnectionService;
import ru.t1.sarychevv.tm.service.LoggerService;
import ru.t1.sarychevv.tm.service.PropertyService;
import ru.t1.sarychevv.tm.service.dto.*;
import ru.t1.sarychevv.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {
    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @Getter
    @NotNull
    private final IProjectDTOService projectDTOService = new ProjectDTOService(connectionService);
    @Getter
    @NotNull
    private final ITaskDTOService taskDTOService = new TaskDTOService(connectionService);
    @Getter
    @NotNull
    private final IProjectTaskDTOService projectTaskDTOService = new ProjectTaskDTOService(projectDTOService,
            taskDTOService);
    @Getter
    @NotNull
    private final IUserDTOService userDTOService = new UserDTOService(propertyService, connectionService,
            projectDTOService, taskDTOService);
    @NotNull
    @Getter
    private final ISessionDTOService sessionDTOService = new SessionDTOService(connectionService);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userDTOService, sessionDTOService);
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    private final ICalcEndpoint calcEndpoint = new CalcEndpoint(this);

    {
        registry(authEndpoint);
        registry(calcEndpoint);
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        ;
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }


    @SneakyThrows
    public void start() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("user");
        user.setPassword("user");
        user.setEmail("user@email.ru");
        @NotNull final UserDTO admin = new UserDTO();
        admin.setLogin("user");
        admin.setPassword("user");
        admin.setEmail("user@email.ru");
        userDTOService.add(user);
        userDTOService.add(admin);

        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        loggerService.info("**TASK-MANAGER IS SHUTTING DOWN**");
    }


}
