package ru.t1.sarychevv.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.model.IRepository;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

    @NotNull
    M removeOneById(@Nullable String id) throws Exception;

}

