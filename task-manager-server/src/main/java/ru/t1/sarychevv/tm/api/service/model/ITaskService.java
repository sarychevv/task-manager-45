package ru.t1.sarychevv.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId,
                @Nullable String name) throws Exception;

    @NotNull
    Task create(@Nullable String userId,
                @Nullable String name,
                @Nullable String description) throws Exception;

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId,
                                  @Nullable String projectId) throws Exception;

    void updateProjectIdById(@Nullable final String userId,
                             @Nullable final String id,
                             @Nullable final String projectId) throws Exception;

}

