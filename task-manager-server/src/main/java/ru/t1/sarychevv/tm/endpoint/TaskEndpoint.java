package ru.t1.sarychevv.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.endpoint.ITaskEndpoint;
import ru.t1.sarychevv.tm.api.service.IServiceLocator;
import ru.t1.sarychevv.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.sarychevv.tm.api.service.dto.ITaskDTOService;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.dto.request.task.*;
import ru.t1.sarychevv.tm.dto.response.task.*;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.enumerated.TaskSort;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskDTOService getTaskService() {
        return serviceLocator.getTaskDTOService();
    }

    @NotNull
    private IProjectTaskDTOService getProjectTaskService() {
        return serviceLocator.getProjectTaskDTOService();
    }

    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull final TaskBindToProjectRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskBindToProjectResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@WebParam(name = REQUEST, partName = REQUEST)
                                                               @NotNull final TaskUnbindFromProjectRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskUnbindFromProjectResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(@WebParam(name = REQUEST, partName = REQUEST)
                                                             @NotNull final TaskChangeStatusByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getTaskService().changeStatusById(userId, id, status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskChangeStatusByIdResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                                   @NotNull final TaskChangeStatusByIndexRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getTaskService().changeStatusByIndex(userId, index, status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskChangeStatusByIndexResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskClearResponse clearTask(@WebParam(name = REQUEST, partName = REQUEST)
                                       @NotNull final TaskClearRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            getTaskService().removeAll(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskClearResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public TaskCreateResponse createTask(@WebParam(name = REQUEST, partName = REQUEST)
                                         @NotNull final TaskCreateRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getTaskService().create(userId, name, description);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskCreateResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskGetByIdResponse getTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                           @NotNull final TaskGetByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getTaskService().findOneById(userId, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskGetByIdResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskGetByIndexResponse getTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                 @NotNull final TaskGetByIndexRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getTaskService().findOneByIndex(userId, index);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskGetByIndexResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskListByProjectIdResponse getTaskByProjectId(@WebParam(name = REQUEST, partName = REQUEST)
                                                          @NotNull final TaskListByProjectIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable List<TaskDTO> tasks = new ArrayList<>();
        try {
            tasks = getTaskService().findAllByProjectId(userId, projectId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskListByProjectIdResponse(tasks);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskListResponse listTask(@WebParam(name = REQUEST, partName = REQUEST)
                                     @NotNull final TaskListRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final TaskSort sort = request.getTaskSort();
        @Nullable List<TaskDTO> tasks = new ArrayList<>();
        if (sort == null) {
            try {
                tasks = getTaskService().findAll(userId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                tasks = getTaskService().findAll(userId, sort.getComparator());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new TaskListResponse(tasks);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                                 @NotNull final TaskRemoveByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getTaskService().removeOneById(userId, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskRemoveByIdResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskRemoveByIndexResponse removeTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull final TaskRemoveByIndexRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getTaskService().removeOneByIndex(userId, index);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskRemoveByIndexResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                                 @NotNull final TaskUpdateByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getTaskService().updateById(userId, id, name, description);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskUpdateByIdResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUpdateByIndexResponse updateTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull final TaskUpdateByIndexRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable TaskDTO task = new TaskDTO();
        try {
            task = getTaskService().updateByIndex(userId, index, name, description);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TaskUpdateByIndexResponse(task);
    }

}
